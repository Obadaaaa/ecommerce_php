<?php include 'inc/header.php' ?>

<?php
$name = $quantity = $selected_part = $nameErr = $quantityErr =$optionsErr=  null;


if (isset($_POST['submit'])) {
    // Validate name
    if (empty($_POST['name'])) {
      $nameErr = 'Name is required';
    } else {
      $name = filter_input(
        INPUT_POST,
        'name',
        FILTER_SANITIZE_FULL_SPECIAL_CHARS
      );
    }
    if (empty($_POST['quantity'])) {
        $quantityErr = 'Quantity is required';
      } else {
        $quantity = filter_input(
          INPUT_POST,
          'quantity',
          FILTER_SANITIZE_NUMBER_INT
        );
      }

      if (empty($_POST['parts'])) {
        $optionsErr = 'please select a part ';
      } else {
    $selected_part = $_POST['parts'];
      }

      if (empty($nameErr) && empty($emailErr) && empty($bodyErr)) {
        mysqli_begin_transaction($conn);

        $productSql = "INSERT INTO products (products_name,quantity) VALUES ('$name','$quantity');";
        if(mysqli_query($conn,$productSql)){
        $product_id = mysqli_insert_id($conn);
        $product_part = "INSERT INTO parts_products (parts_id,products_id) VALUES ('$selected_part','$product_id')";
        if (mysqli_query($conn,$product_part)){
            mysqli_commit($conn);
            header('Location:index.php');
            exit; }
        else {
            mysqli_rollback($conn);
            echo 'Error inserting into parts_products'.mysqli_error($conn);
        }}

    else {
        mysqli_rollback($conn);
        echo 'Error inserting into products: ' . mysqli_error($conn); }    
    }
      }

?>




<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="POST">
      <div class="mb-3">
        <label for="name" class="form-label">Name</label>
        <input type="text" class="form-control <?php echo !$nameErr ?:
          'is-invalid'; ?>" id="name" name="name" placeholder="Enter your name" value="">
         <?php if ($nameErr) echo "<div class='invalid-feedback'>$nameErr</div>"; ?>
      </div>


      <div class="mb-3">
        <label for="quantity" class="form-label"> Quantity</label>
        <input type="text" class="form-control <?php echo !$quantityErr ?:
          'is-invalid'; ?>" id="quantity" name="quantity" placeholder="Enter Product Quantity" value="">
        <?php if ($quantityErr) echo "<div class='invalid-feedback'>$quantityErr</div>"; ?>

      </div>

    <div class = "mb-3">
        <select name="parts" class="form-select <?php echo !$optionsErr ?:'is-invalid';?>">
        <option value="">Select a part</option>

        <?php
        $query = "SELECT id,parts_name FROM parts ";
        $result = mysqli_query($conn, $query);
         while ($row = mysqli_fetch_assoc($result)) {
         echo "<option value='".$row['id']."'>".$row['parts_name']."</option>";
         }
         ?>
        </select>
        <?php if ($optionsErr) echo "<div class='invalid-feedback'>$optionsErr</div>"; ?>

    </div>
    <div class="mb-3">
       <button type="submit" class="btn btn-primary" name="submit">Submit</button>
    </div>
    </form>


<?php include 'inc/footer.php'?>
