<?php include 'inc/header.php'; ?>

<?php
$partswithproducts = 'SELECT parts.parts_name, GROUP_CONCAT(products.products_name) AS products_names
FROM parts
INNER JOIN parts_products ON parts.id = parts_products.parts_id
INNER JOIN products ON parts_products.products_id = products.id
GROUP BY parts.parts_name';
/* $partswithproducts = 'SELECT parts.parts_name, products.products_name
FROM parts
INNER JOIN parts_products ON parts.id = parts_products.parts_id
INNER JOIN products ON parts_products.products_id = products.id;'; */

$result = mysqli_query($conn,$partswithproducts);

?>

<h2>Products and Parts</h2>
    <ul>
 
    <?php while ($row = mysqli_fetch_assoc($result)): ?>
        <li> <?php echo $row['parts_name']; ?>
            <ul>
                <?php $products = explode(',', $row['products_names']);?>
                <?php foreach ($products as $product):?>
                <li><?php echo $product ?></li>
                <?php endforeach; ?>
            </ul>
        </li>
        <?php endwhile; ?>
    </ul>











<?php include 'inc/footer.php' ?>
